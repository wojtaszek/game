## Compilation

    cd Game
    mkdir build
    cd build
    cmake . ../
    make
    test -f libgame.a && echo ok || echo err

## Unit tests
Requires GTest library installed on host machine

    cd Game
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Debug . ../
    make
    ./ut
    # or you can run ctest
    ctest

## Notes from author
Below you will find my general thoughts about what/how other things can be done

1. Using templates for `GameCoinType` - I think it's redundant aldough it shouldn't be a problem if neccessary
2. From my perspective public `GameMachine` methods require threading synchronization
3. Some of unit tests could be optimized considering memory and performance but I don't think it's really important

## Improvements

* download googletest if not present on host machine?
