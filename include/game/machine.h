#pragma once
#include <chrono>
#include <functional>
#include <map>
#include <mutex>
#include <random>
#include <stdexcept>
#include <vector>


using GameInsufficientCredit = std::logic_error;
using GameInsufficientPool = std::logic_error;
using GameInvalidCoin = std::invalid_argument;

using GameCoinType = int;
using GameCoinValidatorType = std::function<bool(GameCoinType)>;
using GameCoins = std::vector<GameCoinType>;
using GameCoinsPocket = std::map<GameCoinType, GameCoinType>;
using GameScoreGeneratorType = std::function<GameCoinType()>;
using GamePossibleScore = std::vector<GameCoinType>;


class GameScoreGenerator {
public:
    GameScoreGenerator(GamePossibleScore score = GamePossibleScore{0, 1, 2, 3, 5, 10, 100});
    GameCoinType operator()();

private:
    using _clock = std::chrono::high_resolution_clock;
    using Generator = std::minstd_rand;
    GamePossibleScore _scores;
    Generator _generator;
    const _clock::time_point _zero = _clock::now();
};


class GameCoinValidator {
public:
    GameCoinValidator(GameCoins coins = GameCoins{1, 2, 5});
    bool operator()(GameCoinType coin);

private:
    GameCoins _valid_coins;
};


class GameMachine {
public:
    GameMachine(GameCoins coins = GameCoins{},
        GameScoreGeneratorType generator = GameScoreGenerator(),
        GameCoinValidatorType validator = GameCoinValidator());

    GameCoinType credit() const;
    GameCoinType pool() const;
    void payin(GameCoinType coin);
    GameCoins payout();
    void play();

private:
    GameCoinsPocket _pocket;
    GameCoinType _credit = 0;
    GameScoreGeneratorType _score;
    GameCoinValidatorType _is_valid;
    mutable std::mutex _mutex;
};
