#include <game/machine.h>
#include <algorithm>
#include <numeric>


GameScoreGenerator::GameScoreGenerator(GamePossibleScore scores): _scores{std::move(scores)} {}


GameCoinType GameScoreGenerator::operator()() {
    _clock::duration d = _clock::now() - _zero;
    _generator.seed(d.count());
    auto n = _generator()%_scores.size(); // should be enough
    return _scores.at(n);
}


GameCoinValidator::GameCoinValidator(GameCoins valid_coins):
    _valid_coins{std::move(valid_coins)} {
    std::sort(_valid_coins.begin(), _valid_coins.end());
}


bool GameCoinValidator::operator()(GameCoinType coin) {
    return std::binary_search(_valid_coins.begin(), _valid_coins.end(), coin);
}


GameMachine::GameMachine(GameCoins coins, GameScoreGeneratorType generator,
        GameCoinValidatorType validator): _score{generator}, _is_valid{validator}, _mutex{} {
    std::lock_guard<std::mutex> _{_mutex};
    for (auto coin: coins)
        if (_is_valid(coin))
            ++_pocket[coin];
        else
            throw GameInvalidCoin("invalid coin value");
}


GameCoinType GameMachine::credit() const {
    std::lock_guard<std::mutex> _{_mutex};
    return _credit;
}


GameCoinType GameMachine::pool() const {
    std::lock_guard<std::mutex> _{_mutex};
    return std::accumulate(_pocket.begin(), _pocket.end(), 0, [](auto s, auto c) {
            return s + c.first*c.second;
        }) - _credit;
}


void GameMachine::payin(GameCoinType coin) {
    std::lock_guard<std::mutex> _{_mutex};
    if (!_is_valid(coin))
        throw GameInvalidCoin("invalid coin value");
    ++_pocket[coin];
    _credit += coin;
}


static auto extract_coins(GameCoinsPocket pocket, GameCoinType amount) {
    GameCoins coins{};
    for (auto coin = pocket.rbegin(); coin != pocket.rend() and amount > 0; ++coin) {
        if (coin->first > amount)
            continue;
        for (auto n = amount/coin->first; n > 0 and coin->second > 0; --n, --coin->second, amount-=coin->first)
            coins.push_back(coin->first);
    }
    if (amount != 0)
        throw GameInsufficientPool("insufficient pool");
    return std::move(coins);
}


GameCoins GameMachine::payout() {
    std::lock_guard<std::mutex> _{_mutex};
    auto pocket = _pocket;
    auto coins = extract_coins(pocket, _credit);
    _pocket = std::move(pocket);
    _credit = 0;
    return std::move(coins);
}


void GameMachine::play() {
    std::lock_guard<std::mutex> _{_mutex};
    auto bet = 1;
    if (_credit < 1)
        throw GameInsufficientCredit("insufficient credit");
    else if (_credit >= 20)
        bet = 20;
    else if (_credit >= 5)
        bet = 5;
    try {
        auto pocket = _pocket;
        auto score = _score();
        extract_coins(pocket, score + _credit);
        _credit -= bet;
        _credit += score;
    } catch (const GameInsufficientPool& ip) {
        throw;
    } catch (...) {
        std::throw_with_nested(std::runtime_error("exception form score function"));
    }
}
