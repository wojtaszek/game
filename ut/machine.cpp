#include <game/machine.h>
#include <gmock/gmock.h>
#include <numeric>

using namespace testing;


struct GameMachineTest: Test {
    GameMachine gm {GameCoins{1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 5, 5, 5, 5, 5},
        []{ return 0; }};

    void SetUp() {
        ASSERT_EQ(gm.credit(), 0);
    }
};


TEST_F(GameMachineTest, PayinValidCoins) {
    GameCoins coins { 1, 2, 5 };
    for (auto coin: coins)
        ASSERT_NO_THROW(gm.payin(coin));
}


TEST_F(GameMachineTest, ThrowsExceptionWhenPayinInvalidCoins) {
    /* we could check all possibilities where: coin != { 1 2 5 }
     * ...but unit tests should be fast...
     */
    GameCoins coins { 0, 3, 4, 6, -1 };
    for (auto coin : coins)
        ASSERT_THROW(gm.payin(coin), GameInvalidCoin);
}


TEST_F(GameMachineTest, ConstructorThrowExceptionWhenInvalidCoins) {
    GameCoins valid_coins{1, 2, 5};
    for (auto coin: valid_coins)
        ASSERT_NO_THROW(GameMachine(GameCoins{coin}, []{return 0;}));

    GameCoins invalid_coins{-1, 0, 3, 4, 6};
    for (auto coin: invalid_coins)
        ASSERT_THROW(GameMachine(GameCoins{coin}, []{ return 0; }), GameInvalidCoin);
}


TEST_F(GameMachineTest, CreditIsSumOfPayments) {
    gm.payin(1);
    ASSERT_EQ(gm.credit(), 1);
    gm.payin(2);
    ASSERT_EQ(gm.credit(), 3);
    gm.payin(1);
    ASSERT_EQ(gm.credit(), 4);
}


TEST_F(GameMachineTest, PayoutReturnsPaidCoins) {
    GameMachine gm{};
    ASSERT_THAT(gm.payout(), SizeIs(0));

    gm.payin(1);
    ASSERT_THAT(gm.credit(), Eq(1));
    ASSERT_THAT(gm.payout(), UnorderedElementsAre(1));
    ASSERT_THAT(gm.payout(), SizeIs(0));

    gm.payin(5);
    gm.payin(2);
    gm.payin(1);
    ASSERT_THAT(gm.credit(), Eq(8));
    ASSERT_THAT(gm.payout(), UnorderedElementsAre(2, 1, 5));
    ASSERT_THAT(gm.payout(), SizeIs(0));
}


TEST_F(GameMachineTest, CannotPlayWhenInsufficientCredit) {
    ASSERT_THROW(gm.play(), GameInsufficientCredit);
    gm.payin(1);
    ASSERT_NO_THROW(gm.play());
}


TEST_F(GameMachineTest, AllowsToPlayForOneCredit) {
    GameMachine gm{GameCoins{}, []{return 0;}};
    gm.payin(1);
    ASSERT_EQ(gm.credit(), 1);
    gm.play();
    ASSERT_EQ(gm.credit(), 0);
    
    while (gm.credit() < 4)
        gm.payin(1);
    ASSERT_EQ(gm.credit(), 4);
    gm.play();
    ASSERT_EQ(gm.credit(), 3);
    ASSERT_THAT(gm.payout(), ElementsAre(1, 1, 1));
}


TEST_F(GameMachineTest, IfItsPossiblePlayForFiveCredits) {
    gm.payin(5);
    ASSERT_EQ(gm.credit(), 5);
    gm.play();
    ASSERT_EQ(gm.credit(), 0);

    while (gm.credit() < 5)
        gm.payin(1);
    ASSERT_EQ(gm.credit(), 5);
    gm.play();
    ASSERT_EQ(gm.credit(), 0);

    while (gm.credit() < 19)
        gm.payin(1);
    ASSERT_EQ(gm.credit(), 19);
    gm.play();
    ASSERT_EQ(gm.credit(), 14);
}


TEST_F(GameMachineTest, IfItsPossiblePlayForTwentyCredits) {
    while (gm.credit() < 20)
        gm.payin(5);
    ASSERT_EQ(gm.credit(), 20);
    gm.play();
    ASSERT_EQ(gm.credit(), 0);
}


TEST_F(GameMachineTest, ThrowsExceptionWhenInsufficientPool) {
    GameMachine gm(GameCoins{}, [] { return 1; });
    gm.payin(1);
    ASSERT_EQ(gm.pool(), 0);
    ASSERT_THROW(gm.play(), GameInsufficientPool);
    ASSERT_EQ(gm.credit(), 1);
    ASSERT_THAT(gm.payout(), ElementsAre(1));
}


TEST_F(GameMachineTest, ThrowsExceptionWhenCannotPayout) {
    GameMachine gm(GameCoins{ 2, 2 }, []{return 5;});
    gm.payin(5);
    ASSERT_THROW(gm.play(), GameInsufficientPool);
    ASSERT_EQ(gm.credit(), 5);
    ASSERT_THAT(gm.payout(), ElementsAre(5));
}


TEST_F(GameMachineTest, LostCoinsGoesToPool) {
    GameCoins coins { 1, 2, 5 };
    auto pool = gm.pool();
    for (auto coin: coins)
        gm.payin(coin);
    while (gm.credit() > 0)
        gm.play();
    ASSERT_EQ(gm.credit(), 0);
    ASSERT_EQ(gm.pool(), std::accumulate(coins.begin(), coins.end(), pool));
}


TEST_F(GameMachineTest, IncreaseCreditWhenSomeoneWon) {
    GameMachine gm{GameCoins{1}, []{return 1;}};
    gm.payin(5);
    ASSERT_NO_THROW(gm.play());
    ASSERT_EQ(gm.credit(), 1);
    ASSERT_THAT(gm.payout(), UnorderedElementsAre(1));
}


TEST_F(GameMachineTest, ThrowNestedExceptionWhenScoreFunctionThrows) {
    auto score_function = []{
        throw std::runtime_error("exception from score funciton");
        return -1;
    };
    GameMachine gm{GameCoins{1,2,5}, score_function};
    gm.payin(1);
    ASSERT_THROW(gm.play(), std::nested_exception);
    ASSERT_EQ(gm.credit(), 1);
    std::string top_message;
    std::string nested_message;
    try {
        gm.play();
    } catch (const std::exception& e) {
        top_message = e.what();
        try {
            std::rethrow_if_nested(e);
        } catch (const std::exception& ne) {
            nested_message = ne.what();
        }
    }
    ASSERT_EQ(top_message, "exception form score function");
    ASSERT_EQ(nested_message, "exception from score funciton");
}


TEST(GameScoreGeneratorTest, ScoreDistributionIsLinear) {
    GamePossibleScore possible_scores{1, 2, 3, 5, 10, 100};
    GameScoreGenerator g(possible_scores);
    std::map<GameCoinType, int> scores;
    constexpr unsigned n = 10000;
    constexpr auto nlb = n - n/10; // -10% 
    constexpr auto nhb = n + n/10; // +10%
    for (unsigned i = 0; i < n*possible_scores.size(); ++i)
        ++scores[g()];
    ASSERT_EQ(scores.size(), possible_scores.size());
    for (auto score: possible_scores) {
        /* WARNING: This is not very deterministic test so it's possible
         * (small but still...) that this will fail!
         */
        ASSERT_GT(scores.at(score), nlb) << "for score: " << score;
        ASSERT_LT(scores.at(score), nhb) << "for score: " << score;
    }
}


TEST(GameCoinValidator, ReturnsTrueWhenValidCoin) {
    GameCoinValidator is_valid; // default is {1,2,5}
    GameCoins valid_coins{1,2,5};
    for (auto coin: valid_coins)
        ASSERT_TRUE(is_valid(coin));
    GameCoins invalid_coins{-1,0,3,4,6};
    for (auto coin: invalid_coins)
        ASSERT_FALSE(is_valid(coin));
}
